package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.flink.RichAsyncFuncDemo;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class FlinkAsyncTest {
    @Test
    public void testGet() throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        DataStream<String> stream = AsyncDataStream.unorderedWait(ds, new RichAsyncFuncDemo(), 1, TimeUnit.SECONDS);
        stream.print();
        env.execute("MyAsyncRichFuncTest");
    }
}
