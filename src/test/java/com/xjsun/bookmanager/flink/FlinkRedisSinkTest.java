package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.flink.sink.RedisExampleMapper;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.redis.RedisSink;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FlinkRedisSinkTest {
    @Test
    public void test() throws Exception {
        FlinkJedisPoolConfig conf = new FlinkJedisPoolConfig.Builder()
                .setHost("127.0.0.1").setPort(6379).build();

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.fromSequence(1, 10)
                .map( x -> Tuple2.of(x.toString(), "RedisSinkTest-"+x.toString()))
                .returns(Types.TUPLE(Types.STRING,Types.STRING))
        .addSink(new RedisSink<>(conf, new RedisExampleMapper()));

        env.execute("RedisSinkTest");
    }
}
