package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.pojo.Book;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichAggregateFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WindowFuncTest {
    // 下面两个测试的需求都是在countWindow(5)里面取最大值
    @Test
    public void testCountWindowApi() throws Exception {
        // name, int
        // mark,99
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        SingleOutputStreamOperator<Tuple2<String, Integer>> map = ds.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                String[] ls = value.split(",");
                return Tuple2.of(ls[0], Integer.valueOf(ls[1]));
            }
        });
        map.keyBy(t -> t.f0).countWindow(5).max(1).print();
        env.execute("CountWindowApi");
    }

    @Test
    public void testFlatMapCountWindowMax() throws Exception {
        // name, int
        // mark,99
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        SingleOutputStreamOperator<Tuple2<String, Integer>> map = ds.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                String[] ls = value.split(",");
                return Tuple2.of(ls[0], Integer.valueOf(ls[1]));
            }
        });
        map.keyBy(0).flatMap(new CountWindowMax()).print();
        env.execute("FlatMapCountWindowMax");

    }

    /*
    与前面两个函数不同，此处需求是，返回
     */
    @Test
    public void testCountWindowMaxGetValueRes() throws Exception {
        // name, int
        // mark,99
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        SingleOutputStreamOperator<Tuple2<String, Integer>> map = ds.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                String[] ls = value.split(",");
                return Tuple2.of(ls[0], Integer.valueOf(ls[1]));
            }
        });
        map.keyBy(t -> t.f0).countWindow(5).max(1).print();
        env.execute("CountWindowApi");
    }
}
