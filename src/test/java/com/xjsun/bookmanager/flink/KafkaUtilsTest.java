package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.utils.FlinkKafkaUtils;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/12/28 21:14
 */
@SpringBootTest
public class KafkaUtilsTest {
    private static final String winP = "E:\\project\\bookmanager\\src\\test\\java\\com\\xjsun\\bookmanager\\flink\\KafkaTest.properites";
    private static final String macP = "/Users/sunxingjie/projects/book-manager-spring-boot-learning/src/test/java/com/xjsun/bookmanager/flink/KafkaTest.properites";

    @Test
    public void test() throws IOException {
        ParameterTool params = ParameterTool.fromPropertiesFile(winP);
        String tps = params.getRequired("topics");
        System.out.println(tps);
    }

    @Test
    public void envChangeTest1() {
        long v1 = FlinkKafkaUtils.getEnv().getCheckpointConfig().getCheckpointInterval();
        StreamExecutionEnvironment.getExecutionEnvironment().enableCheckpointing(5000);
        long v2 = FlinkKafkaUtils.getEnv().getCheckpointConfig().getCheckpointInterval();

        Assertions.assertNotEquals(v1, v2);
    }
    @Test
    public void envChangeTest2() {

        StreamExecutionEnvironment env1 = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamExecutionEnvironment env2 = StreamExecutionEnvironment.getExecutionEnvironment();

        Assertions.assertEquals(env1,env2);
    }
    @Test
    public void getKafkaSourceTest() throws Exception {
        ParameterTool params = ParameterTool.fromPropertiesFile(macP);
        DataStream<String> ds = FlinkKafkaUtils.<String>getKafkaDataStream(params, SimpleStringSchema.class);
        ds.print();
        FlinkKafkaUtils.getEnv().execute("getKafkaSourceTest");

    }
}
