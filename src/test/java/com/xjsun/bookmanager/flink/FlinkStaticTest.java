package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.flink.FlinkTest;
import com.xjsun.bookmanager.flink.RichFunctionDemo;
import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
public class FlinkStaticTest {
    @Autowired
    RedisTemplate redisTemplate;

//    @Test
//    public void test1() throws Exception {
//        FlinkTest.test();
//    }

    @Test
    public void initRedis() {
        Author a1 = new Author(1, "Bob");
        Author a2 = new Author(2, "Tom");
        Author a3 = new Author(3, "John");

        redisTemplate.opsForValue().set("1", a1);
        redisTemplate.opsForValue().set("2", a2);
        redisTemplate.opsForValue().set("3", a3);
        FlinkJedisPoolConfig conf = new FlinkJedisPoolConfig.Builder()
                .setHost("127.0.0.1")
                .setPort(6379)
                .setMaxTotal(10)
                .build();
    }

    @Test
    public void testRichFunc() throws Exception {
        // timestamp, bookId, authorId
        // 1000,99,3
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        SingleOutputStreamOperator<Book> bookStream = ds.map(new RichFunctionDemo());
        bookStream.print();
        env.execute("MyRichFuncTest");
    }

    // 试了一下  2次重启次数   输入第三个exception的时候测试结束；前面两次都是悄悄重启，控制台没有输出日志
    @Test
    public void testRestart() throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(2,2000));
//        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        env.getCheckpointConfig().setCheckpointStorage(FlinkConstConfigs.CHECK_POINT_DIR_LOCAL);
        env.setStateBackend(new HashMapStateBackend());
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        KeyedStream<Tuple2<String, Integer>, String> keyedStream = ds.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                if (value.equals("exception")) {
                    System.out.println("exception here");
                    throw new RuntimeException("模拟：发生了错误");
                }
                return Tuple2.of(value, 1);
            }
        }).keyBy(t -> t.f0);
        keyedStream.sum(1).print(); // flink自带的sum也是有state的，如果开启了本地检查点就可以重启后接着之前的state累加
        env.execute("testRestart1");
    }

    // 配合带有state的累加任务 测试重启后state的容错作用
    // word count 累加过程中出错重启
    @Test
    public void testRestart2() throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(2,2000));
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getCheckpointConfig().setCheckpointStorage(FlinkConstConfigs.CHECK_POINT_DIR_LOCAL);
        env.setStateBackend(new HashMapStateBackend());
        DataStreamSource<String> ds = env.socketTextStream("localhost", 8888);
        KeyedStream<Tuple2<String, Integer>, String> keyedStream = ds.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                if (value.equals("exception")) {
                    System.out.println("exception here");
                    throw new RuntimeException("模拟：发生了错误");
                }
                return Tuple2.of(value, 1);
            }
        }).keyBy(t -> t.f0); // 对word做分组

        keyedStream.map(new RichMapFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
            private transient ValueState<Integer> curNum;

            @Override
            public void open(Configuration parameters) throws Exception {
                ValueStateDescriptor<Integer> descriptor = new ValueStateDescriptor<>("current num of the word", Types.INT);
                curNum = getRuntimeContext().getState(descriptor);
            }

            @Override
            public Tuple2<String, Integer> map(Tuple2<String, Integer> value) throws Exception {
                Integer temp = curNum.value();
                if (temp==null) temp = 0;
                temp += value.f1;
                curNum.update(temp);
                return Tuple2.of(value.f0, temp);
            }
        }).print();

        env.execute("testRestart2");
    }

}
