package com.xjsun.bookmanager.flink;

import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class KeyedStateTest {
    @Test
    public void test() {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5);
        env.getCheckpointConfig().setCheckpointStorage(FlinkConstConfigs.CHECK_POINT_DIR_LOCAL);
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.socketTextStream(FlinkConstConfigs.LOCAL_HOST, FlinkConstConfigs.SOCKET_STREAM_PORT);


    }
}
