package com.xjsun.bookmanager.redis;

import com.xjsun.bookmanager.utils.TestAopOnComponent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ToRedisAspectTest {
    @Autowired
    TestAopOnComponent taoc;

    @Test
    public void test() {

        taoc.testAopToRedisCache(1);
    }

    @Test
    public void test2() {
        taoc.testAopToRedisCachePoint2(666);
    }

    @Test
    public void test3() {
        taoc.testAopToRedisCachePoint3(333);
    }
}
