package com.xjsun.bookmanager.redis;

import com.xjsun.bookmanager.pojo.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/8 18:03
 */
@SpringBootTest
@SuppressWarnings("all")
public class RedisTest {
    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplate;

    @Test
    public void contextLoads() {
        redisTemplate.opsForValue().set("123", "666");
        System.out.println(redisTemplate.opsForValue().get("123"));

        Author a = new Author(10,"theAuthor");
        redisTemplate.opsForValue().set("a", a);
        System.out.println(redisTemplate.opsForValue().get("a"));

    }

    @Test
    public void testDeseri(){
        Author a = (Author) redisTemplate.opsForValue().get("a");
        System.out.println(a);
    }
}
