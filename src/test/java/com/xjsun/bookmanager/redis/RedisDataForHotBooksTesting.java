package com.xjsun.bookmanager.redis;

import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.redis.keyPrefix.BookPrefix;
import com.xjsun.bookmanager.redis.service.RedisServiceSimple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@SpringBootTest
public class RedisDataForHotBooksTesting {
    @Autowired
    RedisServiceSimple redisServiceSimple;
    private String prefix = BookPrefix.getBooksDetailPrefix().getPrefix();

    @Test
    public void setValuesForHotBooks(){
        ArrayList<Book> books = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Book temp = new Book();
            temp.setId(100+i);
            temp.setTitle(String.format("Title of redis test book %d",i));
            books.add(temp);
        }

        for (Book b : books) {
            redisServiceSimple.set(prefix, Integer.toString(b.getId()), b);
        }
    }

    @Test
    public void getValueTest(){
        Book res = redisServiceSimple.get(prefix,"102", Book.class);
        System.out.println(res.getTitle());
    }
}
