package com.xjsun.bookmanager;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xjsun.bookmanager.controller.AuthorsController;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.pojo.Book;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/22 16:09
 */
@SpringBootTest
public class Log4jTest {
    @Autowired
    BookMapper bookMapper;
    private static Logger logger = LogManager.getLogger(AuthorsController.class.getName());

    @Test
    public void testC2B() {
        PageHelper.startPage(2, 5);
        List<Book> books = bookMapper.findAll();
        PageInfo<Book> pageInfo = new PageInfo<>(books, 5);
        for (Book b : pageInfo.getList()) {
            System.out.println(b);
        }
        assert pageInfo.getList().size() == 5;
        // 可以看到 根据配置  info是不会输出的  warn和error会输出
        System.out.println("pageNum:" + pageInfo.getPageNum());
        logger.info("pageNum:" + pageInfo.getPageNum());
        logger.warn("pageNum:" + pageInfo.getPageNum());
        logger.error("pageNum:" + pageInfo.getPageNum());
    }

}
