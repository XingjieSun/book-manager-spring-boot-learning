package com.xjsun.bookmanager.mvc;

import cn.hutool.core.text.StrFormatter;
import com.xjsun.bookmanager.mapper.AuthorMapper;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/16 14:22
 */
@SpringBootTest
public class BookSelectingTest {
    @Autowired
    BookMapper bookMapper;
    @Autowired
    AuthorMapper authorMapper;

    //    @Test
    public void selectTest1() {
        System.out.println("Test Book\n");
        int bId = 500, aId = 500;
        String bName = "人类";
        System.out.println("测试查询id=" + bId + "的book");
        Book b = bookMapper.getBookById(bId);
        assert b != null;
        System.out.println(b);

        System.out.println("测试查询所有book");
        List<Book> books = bookMapper.findAll();
        assert books.size() != 0;
        System.out.println("book 总数" + books.size());

        Author a = authorMapper.getAuthorById(aId);
        assert a != null;
        books = bookMapper.getBookListByAuthor(a);
        System.out.println(books);
        assert books.size() != 0;


        books = bookMapper.getBookByName(bName);
        System.out.println(books);
        assert books.size() != 0;

        List<Integer> idLs = new ArrayList<>();
        idLs.add(500);
        idLs.add(501);
        books = bookMapper.getBookListByIdSet(idLs);
        System.out.println(books);
        assert books.size() != 0;
    }

    @Test
    public void multipleSelectsTest() {
        List<Integer> idLs = new ArrayList<>();
        idLs.add(500);
        idLs.add(501);
        Integer bId = 500;
        Integer aId = 509;
        String name1 = "尼尔";
        String name2 = "海贼";
        String authorName = "Koei Tecmo";
        String msg = "\nTest:(aId:{},aName:{},bName:{},idSet:{})";
        System.out.println(StrFormatter.format(msg, aId, authorName, name1, idLs));

        System.out.println(StrFormatter.format(msg, aId, null, null, null));
        List<Book> books = bookMapper.getBook(aId, null, null, null);
        System.out.println(books);
        assert books.size() != 0;

        System.out.println(StrFormatter.format(msg, null, authorName, null, null));
        books = bookMapper.getBook(null, authorName, name2, null);
        System.out.println(books);
        assert books.size() != 0;

//        System.out.println("(null,null,"+name1+",null)");
        System.out.println(StrFormatter.format(msg, null, null, name1, null));
        books = bookMapper.getBook(null, null, name1, null);
        System.out.println(books);
        assert books.size() != 0;


//        System.out.println("(null,null,null,idLs)");
        System.out.println(StrFormatter.format(msg, null, null, null, idLs.toString()));
        books = bookMapper.getBook(null, null, null, idLs);
        System.out.println(books);
        assert books.size() != 0;
    }
}
