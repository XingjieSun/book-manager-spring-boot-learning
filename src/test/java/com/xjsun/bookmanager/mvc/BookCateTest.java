package com.xjsun.bookmanager.mvc;

import com.xjsun.bookmanager.mapper.BookCategoryMapper;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.pojo.BookCategory;
import com.xjsun.bookmanager.pojo.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/21 17:48
 */
@SpringBootTest
public class BookCateTest {
    @Autowired
    private BookCategoryMapper bcMapper;

    @Test
    public void test(){
        int searchBCId=9;
        int bookId=12;
        int cateId=1;
        List<BookCategory> bcList =bcMapper.findAll();
        System.out.println("findALL测试:"+bcList.size());
        assert bcList.size()>0;

        BookCategory bc = bcMapper.getBookCategoryById(searchBCId);
        System.out.println("id测试:"+bc.toString());


        Category c = new Category();
        c.setId(cateId);
        List<Integer> bList= bcMapper.getBookIdListByCategory(c);
        System.out.println("book测试:"+bList.size()); // 2

        Book b = new Book();
        b.setId(bookId);
        List<Category> cList= bcMapper.getCategoryListByBook(b);
        System.out.println("Category测试:"+cList.size()); // 2

        int res=0;
        bc.setCategoryId(3);
        res = bcMapper.updateBookCategory(bc);
        System.out.println("更新设置:"+res);

        bc= new BookCategory(0,5,3);
        res = bcMapper.addBookCategory(bc);
        System.out.println("插入测试："+res);

        bcList =bcMapper.findAll();
        res = bcList.get(bcList.size()-1).getId();
        System.out.println("列表默认记录id："+res+"即将删除");
        res = bcMapper.deleteBookCategory(res);
        System.out.println("删除测试："+res);

    }
}
