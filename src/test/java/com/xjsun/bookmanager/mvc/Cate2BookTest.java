package com.xjsun.bookmanager.mvc;

import com.xjsun.bookmanager.mapper.BookCategoryMapper;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.mapper.CategoryMapper;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.pojo.BookCategory;
import com.xjsun.bookmanager.pojo.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/22 16:09
 */
@SpringBootTest
public class Cate2BookTest {
    @Autowired
    BookMapper bookMapper;
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    BookCategoryMapper bcMapper;
    @Test
    public void testC2B(){
        List<Category> categoryList = categoryMapper.findAll();
        List<Integer> bookIdList = bcMapper.getBookIdListByCategory(categoryList.get(1));
        List<Book> bookList = bookMapper.getBookListByIdSet(bookIdList);
        for (Book b : bookList){
            System.out.println(b);
        }
    }
    @Test
    public void testB2C(){
        List<Book> bookList = bookMapper.findAll();
        List<Category> categoryList = bcMapper.getCategoryListByBook(bookList.get(0));
        for (Category c: categoryList){
            System.out.println(c);
        }
    }
}
