package com.xjsun.bookmanager.mvc;

import com.xjsun.bookmanager.mapper.*;
import com.xjsun.bookmanager.pojo.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@SpringBootTest
class BookmanagerApplicationTests {
    @Autowired
    DataSource dataSource;
    @Autowired
    UserMapper userMapper;
    @Autowired
    BookMapper bookMapper;
    @Autowired
    AuthorMapper authorMapper;
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    BookCategoryMapper bcMapper;
    @Test
    void contextLoads(){
//        testBook();
//        testAuthor();
//        testCategory();
        testBookCategory();
    }


    @Test
    void testBook() {
        System.out.println("Test Book\n");

        System.out.println("测试查询id=3的book");
        Book b = bookMapper.getBookById(4);

        System.out.println(b);

        System.out.println("测试更新id=3的book");
        b.setDate("222");
        b.setAuthor(new Author(2,"xxx"));
        int res1= bookMapper.updateBook(b);
        System.out.println(res1);

        System.out.println("测试查询所有book");
        List<Book> books= bookMapper.findAll();
        assert books.size()!=0;
        System.out.println(books.size());
        Author a = authorMapper.getAuthorById(2);
        books = bookMapper.getBookByName("bookInsert");
        if(books.size()==0){
            System.out.println("不存在 bookInsert  测试插入");
            bookMapper.addBook(new Book(1,"bookInsert",a,"1234",null,""));
        }
        else{
            System.out.println("存在 bookInsert  测试删除");
            books=bookMapper.getBookListByAuthor(a);
            System.out.println("测试getBookListByAuthor 查到记录："+books.size());
            for(Book temp:books){
                bookMapper.deleteBook(temp.getId());
                System.out.println("测试删除成功");
            }
        }
    }

    @Test
    public void testAuthor(){
        System.out.println("测试查询id=1的author");
        Author a = authorMapper.getAuthorById(1);
        assert a!=null;
        System.out.println(a.toString());

        System.out.println("测试查询所有author");
        List<Author> authors= authorMapper.findAll();
        assert authors.size()!=0;
        System.out.println(authors.size());

        System.out.println("测试更改");
        a.setName("fromTest");
        int res= authorMapper.updateAuthor(a);
        System.out.println(res);

        System.out.println("测试查询name为testAdd的作者");
        a = authorMapper.getAuthorByName("testAdd");
        if (a==null){
            System.out.println("测试添加");
            res = authorMapper.addAuthor(new Author(1,"testAdd"));
            System.out.println(res);
        }
        else {
            System.out.println("测试删除");
            res = authorMapper.deleteAuthor(a.getId());
            System.out.println(res);
        }
    }
    @Test
    public void testCategory(){
        System.out.println("测试查询id=1的类别");
        Category a = categoryMapper.getCategoryById(1);
        assert a!=null;
        System.out.println(a.toString());

        System.out.println("测试查询所有类别");
        List<Category> authors= categoryMapper.findAll();
        assert authors.size()!=0;
        System.out.println(authors.size());

        System.out.println("测试更改");
        a.setName("fromTest");
        int res= categoryMapper.updateCategory(a);
        System.out.println(res);

        System.out.println("测试查询name为testAdd的类别");
        a = categoryMapper.getCategoryByName("testAdd");
        if (a==null){
            System.out.println("测试添加");
            res = categoryMapper.addCategory(new Category(1,"testAdd"));
            System.out.println(res);
        }
        else {
            System.out.println("测试删除");
            res = categoryMapper.deleteCategory(a.getId());
            System.out.println(res);
        }
    }
    @Test
    void testBookCategory(){
        List<BookCategory> bcList =bcMapper.findAll();
        System.out.println("findALL测试:"+bcList.size());

        BookCategory bc = bcMapper.getBookCategoryById(6);
        System.out.println("id测试:"+bc.toString());


        Category c = new Category();
        c.setId(1);
        List<Integer> bList= bcMapper.getBookIdListByCategory(c);
        System.out.println("book测试:"+bList.size()); // 2

        Book b = new Book();
        b.setId(4);
        List<Category> cList= bcMapper.getCategoryListByBook(b);
        System.out.println("Category测试:"+cList.size()); // 2

        int res=0;
//        bc.setId(8);
        bc.setCategoryId(3);
        res = bcMapper.updateBookCategory(bc);
        System.out.println("更新设置:"+res);

        bc= new BookCategory(0,5,3);
        res = bcMapper.addBookCategory(bc);
        System.out.println("插入测试："+res);

        res = bcMapper.deleteBookCategory(10);
        System.out.println("删除测试："+res);


    }
    @Test
    void testDatasource() throws SQLException {
        System.out.println(dataSource.getClass());
        System.out.println(dataSource.getConnection());

        int numUsers = userMapper.findAll().size();
        if (numUsers==0){
            System.out.println("插入三个用户数据");
            userMapper.addUser(new User(1,"a123","123"));
            userMapper.addUser(new User(1,"b123","123"));
            userMapper.addUser(new User(1,"c123","123"));
            System.out.println(userMapper.findAll().size());
        }
    }
}
