package com.xjsun.bookmanager;

import com.xjsun.bookmanager.service.ApicInfoDto;
import com.xjsun.bookmanager.service.NewDiskManager;

import java.nio.file.Paths;
import java.util.Map;

import static com.xjsun.bookmanager.utils.NioFileChannelUtil.readTxt;
import static com.xjsun.bookmanager.utils.NioFileWalker.getPossibleDrives;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2022/4/12 16:06
 */
public class FlieOpTest {
    public static void main(String[] args) {
//        testWalker();


        String s = readTxt(Paths.get("G:\\NewDisk整理结果\\DiskName.txt"));
        System.out.println(s);

        ApicInfoDto dto = NewDiskManager.aPicName2InfoDict("ABP-232_あやみ旬果_NoTags_LX-1T.jpg");
        System.out.println(dto);

        Map<String, String> possibleDrives = getPossibleDrives();
        for (String key : possibleDrives.keySet()) {
            NewDiskManager m = new NewDiskManager(key, possibleDrives.get(key));
            m.generateAllIndexes();
        }

    }
}
