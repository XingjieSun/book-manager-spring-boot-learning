package com.xjsun.bookmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

import static java.lang.Thread.sleep;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/7 19:55
 */
@Service
public class MyMails {
    @Autowired
    JavaMailSenderImpl mailSender;

    @Async
    public void testAsync() {
        System.out.println("In testAsync");
        try {
            sleep(7000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    public void sendMail(String subject, String text, Boolean multipart) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, multipart);
        // 正文
        helper.setSubject(subject);
        helper.setText(text);
        // 附件
        helper.addAttachment("10.jpg", new File("C:\\Users\\s1522\\Pictures\\云韵\\1.jpg"));

        helper.setTo("y30190794@mail.ecust.edu.cn");
        helper.setFrom("sunxingjieecust@163.com");

        mailSender.send(mimeMessage);
    }


}
