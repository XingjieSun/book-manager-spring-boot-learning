package com.xjsun.bookmanager.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2022/4/12 19:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApicInfoDto {
    public String idf;
    public String astar;
    public String tags;
    public String diskName;

}
