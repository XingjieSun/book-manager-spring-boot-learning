package com.xjsun.bookmanager.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2022/4/12 17:52
 */
public class NewDiskManager {
    public static final String COVERS_DIR_NAME = "__所有封面__";
    public static final String NO_TAGS = "NoTags";
    public static final String TAG_SEPARATOR = "；";
    public static final Pattern APIC_PATTERN = Pattern.compile("(.*)_(.*)_(.*)_(.*)\\.jpg");

    protected Path rootDir;
    protected String diskName;
    protected Path coversDir;
    protected int apicCount;
    Map<String, List<ApicInfoDto>> indexOnTags = null;
    Map<String, List<ApicInfoDto>> indexOnStars = null;
    Map<String, ApicInfoDto> indexOnIdf = null;

    public NewDiskManager(String rootDir, String diskName) {
        this.rootDir = Paths.get(rootDir);
        this.diskName = diskName;
        this.coversDir = this.rootDir.resolve(COVERS_DIR_NAME);
        if (!Files.exists(this.coversDir)) {
            try {
                Files.createDirectories(this.coversDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initIndexes() {
        indexOnTags = new HashMap<>(8);
        indexOnStars = new HashMap<>(16);
        indexOnIdf = new HashMap<>(128);
    }

    public void generateAllIndexes() {
        long startT = System.currentTimeMillis();
        initIndexes();
        final int[] count = {0};
        ArrayList<ApicInfoDto> withoutTagsList = new ArrayList<>(64);
        try (Stream<Path> apics = Files.walk(this.coversDir)) {
            apics.forEach(new Consumer<Path>() {
                private void addToIndex(Map<String, List<ApicInfoDto>> map, String id, ApicInfoDto dto) {
                    if (id == null) {
                        return;
                    }
                    map.putIfAbsent(id, new ArrayList<>(2));
                    map.get(id).add(dto);
                }

                @Override
                public void accept(Path path) {
                    ApicInfoDto dto = aPicName2InfoDict(path.getFileName().toString());
                    if (dto == null) {
                        return;
                    }
                    count[0] += 1;
                    if (dto.tags != null) {
                        if (dto.tags.equals(NO_TAGS)) {
                            withoutTagsList.add(dto);
                        } else {
                            for (String tag : dto.tags.split(TAG_SEPARATOR)) {
                                addToIndex(indexOnTags, tag, dto);
                            }
                        }
                    }
                    if (dto.astar != null) {
                        addToIndex(indexOnStars, dto.astar, dto);
                    }
                    if (dto.idf != null) {
                        indexOnIdf.putIfAbsent(dto.idf, dto);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        indexOnTags.put(NO_TAGS, withoutTagsList);
        apicCount = count[0];
        long endT = System.currentTimeMillis();
        System.out.println("Apic索引生成");
        System.out.println("Apic数量：" + apicCount);
        System.out.println("耗时：" + (endT - startT) + "ms\n");
    }


    // 写着玩 复习一下Function函数式编程
    public Map<String, List<ApicInfoDto>> generateIndex(Function<ApicInfoDto, String> getIndex) {
        Map<String, List<ApicInfoDto>> res = new HashMap<>(4);
        try (Stream<Path> apics = Files.list(this.coversDir)) {
            apics.forEach(new Consumer<Path>() {
                @Override
                public void accept(Path path) {
                    ApicInfoDto dto = aPicName2InfoDict(path.getFileName().toString());
                    String index = getIndex.apply(dto);
                    if (index != null) {
                        res.putIfAbsent(index, new ArrayList<>(2));
                        res.get(index).add(dto);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }


    public static ApicInfoDto aPicName2InfoDict(String aPicName) {
        Matcher matchObj = APIC_PATTERN.matcher(aPicName);
        if (!matchObj.matches()) {
            System.out.println("APic提取信息失败, 格式不符：" + aPicName);
            return null;
        }
        return new ApicInfoDto(matchObj.group(1), matchObj.group(2), matchObj.group(3), matchObj.group(4));
    }

}
