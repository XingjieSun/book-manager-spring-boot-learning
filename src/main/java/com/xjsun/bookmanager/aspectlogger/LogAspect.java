package com.xjsun.bookmanager.aspectlogger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;


@Component
@Aspect
@EnableAspectJAutoProxy
public class LogAspect {
    @Pointcut("@annotation(com.xjsun.bookmanager.aspectlogger.MyAspectLogger)")
    public void point(){}

    @Around("point()")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("Enter into method: "+joinPoint.getSignature().getName());
        // 执行切面的方法
        Object res = joinPoint.proceed();
        // 执行切面的方法
        System.out.println("After method: "+joinPoint.getSignature().getName());
        return res;
    }

    @AfterReturning(pointcut = "point()", returning = "res")
    public void doAfterReturning(JoinPoint joinPoint, Object res) {
        System.out.println(joinPoint.getSignature().getName()+ " returns: "+res);
    }


    @AfterThrowing(value = "point()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        System.out.println(joinPoint.getSignature().getName()+ " fails: "+e.getMessage());
    }

}
