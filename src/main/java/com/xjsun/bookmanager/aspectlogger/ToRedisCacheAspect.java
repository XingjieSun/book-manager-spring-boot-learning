package com.xjsun.bookmanager.aspectlogger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Aspect
public class ToRedisCacheAspect {
    public static final String REDIS_HASH_PREFIX = "Method_";

    @Resource(name = "redisTemplate")
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private RedisTemplate<String, String> template;

    @Pointcut(value = "@annotation(com.xjsun.bookmanager.aspectlogger.ToRedisCache)")
    public void point(){}

    @Pointcut(value = "execution(* com.xjsun.bookmanager.utils.TestAopOnComponent.testAopToRedisCachePoint2(..))")
    public void point2(){}

    @AfterReturning(pointcut = "point()", returning = "res")
    public Object toCache(JoinPoint jp, Object res) {

        String k = REDIS_HASH_PREFIX+jp.getSignature().getName();
        String curTime = Long.toString(System.currentTimeMillis());
        template.opsForHash().put(k,curTime, res.toString());
        System.out.println(k);
        return res;
    }

    @Around("point2() && args(id)")
    public Object toCache2(ProceedingJoinPoint pjp, int id){
        System.out.println("进入切面toCache2");
        Object res = null;
        long start = System.currentTimeMillis();
        try {
            res = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        long end = System.currentTimeMillis();
        String out = "运行时间："+(end-start)+";输入:"+id+";对象：";
        if (res==null) {
            out+="null";
        } else {
            out+=res.toString();
        }
        System.out.println(out);

        return res;
    }
}
