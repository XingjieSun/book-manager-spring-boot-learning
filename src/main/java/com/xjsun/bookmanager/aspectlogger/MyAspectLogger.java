package com.xjsun.bookmanager.aspectlogger;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAspectLogger {
}
