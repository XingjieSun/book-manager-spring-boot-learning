package com.xjsun.bookmanager.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 1:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Author Entity")
public class Author {
    private int id;
    private String name;

}
