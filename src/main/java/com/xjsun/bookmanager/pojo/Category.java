package com.xjsun.bookmanager.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 1:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Category Entity")
public class Category implements Serializable {
    private int id;
    private String name;
}
