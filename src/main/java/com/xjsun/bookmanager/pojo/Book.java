package com.xjsun.bookmanager.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 1:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Book Entity")
public class Book {
    private int id;
    @ApiModelProperty("标题")
    private String title;
    private Author author;
    private String date;
    private List<Category> categoryList;
    private String url;
    public String getCategoryString(){
        if (this.categoryList==null || this.categoryList.size()==0){
            return "";
        }
        else if (this.categoryList.size()==1){
            return this.categoryList.get(0).getName();
        }
        else{
            // 加逗号 ,
            int maxSize = this.categoryList.size();
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<maxSize-1;i++){
                sb.append(this.categoryList.get(i).getName());
                sb.append(", ");
            }
            sb.append(this.categoryList.get(maxSize-1).getName());
            return sb.toString();
        }
    }
}
