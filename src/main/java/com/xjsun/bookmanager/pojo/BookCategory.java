package com.xjsun.bookmanager.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 21:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookCategory {
    private int id;
    private int bookId;
    private int categoryId;

    public BookCategory(Book b, Category c){
        this.id=0;
        this.bookId=b.getId();
        this.categoryId=c.getId();
    }
}
