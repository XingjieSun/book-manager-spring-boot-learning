package com.xjsun.bookmanager.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/18 21:55
 */
@Data
@NoArgsConstructor
@ApiModel("用户表")
public class User {
    private int id;
    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("密码")
    private String pwd;

    public User(int id, String name, String pwd) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
    }
}
