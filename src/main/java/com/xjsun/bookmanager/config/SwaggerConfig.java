package com.xjsun.bookmanager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/5 18:45
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SPRING_WEB)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xjsun.bookmanager.controller"))
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("Xj Sun", "", "y30190794@mail.ecust.edu.cn");
        return new ApiInfo("BookManager",
                "My Springboot Project",
                "1.0",
                "urn:tos",
                contact,
                "GPL-3.0",
                "https://www.gnu.org/licenses/gpl-3.0.txt",
                new ArrayList()
        );
    }
}
