package com.xjsun.bookmanager.config;

import lombok.Data;
import org.springframework.stereotype.Repository;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/19 13:43
 */
@Repository
@Data
public class ProjectInfo {
    private final String projectName= "Strange Library";
    private final String loginDescription= "一个图书管理网站";
    private final String registerDescription= "欢迎注册~";


}
