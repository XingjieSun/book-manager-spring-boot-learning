package com.xjsun.bookmanager.mapper.impl;

import com.xjsun.bookmanager.mapper.CategoryMapper;
import com.xjsun.bookmanager.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/4/11 21:01
 */
//@CacheConfig(cacheNames = "Redis")
@Repository
@EnableCaching
public class CategoryMapperImpl implements CategoryMapper {
    @Autowired
    CategoryMapper categoryMapper;


    @Override
    public List<Category> findAll() {
        return categoryMapper.findAll();
    }

    @Cacheable(value = "Category", key = "#id")
    @Override
    public Category getCategoryById(int id) {
        return categoryMapper.getCategoryById(id);
    }

    @Override
    public Category getCategoryByName(String name) {
        return categoryMapper.getCategoryByName(name);
    }

    @Override
    public int addCategory(Category category) {
        return categoryMapper.addCategory(category);
    }

    @Override
    public int updateCategory(Category category) {
        return categoryMapper.updateCategory(category);
    }

    @Override
    public int deleteCategory(int id) {
        return categoryMapper.deleteCategory(id);
    }
}
