package com.xjsun.bookmanager.mapper;


import com.xjsun.bookmanager.pojo.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Category Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 13:27
 */
@Mapper
@Repository
public interface CategoryMapper {
    List<Category> findAll();

    Category getCategoryById(int id);
    Category getCategoryByName(String name);

    int addCategory(Category category);

    int updateCategory(Category category);

    int deleteCategory(int id);
}
