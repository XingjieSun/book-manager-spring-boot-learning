package com.xjsun.bookmanager.mapper;

import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 13:13
 */
@Mapper
@Repository
public interface BookMapper {
    List<Book> findAll();
    List<Book> getBookListByAuthor(Author author);
    List<Book> getBookByName(String name);
    List<Book> getBookListByIdSet(List<Integer> idSet);

    List<Book> getBook(Integer authorId, String author, String name, List<Integer> idSet);

    Book getBookById(int id);



    int addBook(Book book);

    int updateBook(Book book);

    int deleteBook(int id);

}
