package com.xjsun.bookmanager.mapper;

import com.xjsun.bookmanager.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/18 22:25
 */
@Mapper
@Repository
public interface UserMapper {
    List<User> findAll();

    User getUserById(int id);

    User getUserByName(String name);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int id);

}
