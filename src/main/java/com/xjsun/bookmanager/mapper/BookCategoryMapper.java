package com.xjsun.bookmanager.mapper;

import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.pojo.BookCategory;
import com.xjsun.bookmanager.pojo.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author s1522
 * @version 1.0
 * @bookCategory Xingjie Sun
 * @date 2021/2/20 21:34
 */
@Mapper
@Repository
public interface BookCategoryMapper {
    List<BookCategory> findAll();

    BookCategory getBookCategoryById(int id);
    List<Integer> getBookIdListByCategory(Category c);
    List<Category> getCategoryListByBook(Book b);

    int addBookCategory(BookCategory bookCategory);

    int updateBookCategory(BookCategory bookCategory);

    int deleteBookCategory(int id);
}
