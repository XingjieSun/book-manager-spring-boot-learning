package com.xjsun.bookmanager.mapper;

import com.xjsun.bookmanager.pojo.Author;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/20 13:27
 */
@Mapper
@Repository
public interface AuthorMapper {
    List<Author> findAll();

    Author getAuthorById(int id);
    Author getAuthorByName(String name);

    int getAuthorIdByName(String name);

    List<String> getBookTitlesByAuthorId(int authorId);

    int addAuthor(Author author);

    int updateAuthor(Author author);

    int deleteAuthor(int id);
}
