package com.xjsun.bookmanager.redis.keyPrefix;

public abstract class BasePrefix {
    private String prefix;

    public BasePrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * 被继承 得到各种实体获取其redis key的类
     * @return 子类的类名:子类自定义的前缀
     */
    public String getPrefix(){
        return getClass().getSimpleName() + ":" +this.prefix+":";
    }
}
