package com.xjsun.bookmanager.redis.keyPrefix;

public class BookPrefix extends BasePrefix {
    private BookPrefix(String prefix) {
        super(prefix);
    }

    public static BookPrefix getBooksListPrefix() {
        return new BookPrefix("bl");
    }

    public static BookPrefix getBooksDetailPrefix() {
        return new BookPrefix("bd");
    }
}
