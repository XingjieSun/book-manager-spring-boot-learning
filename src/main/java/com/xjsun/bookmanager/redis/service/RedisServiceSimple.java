package com.xjsun.bookmanager.redis.service;

import com.xjsun.bookmanager.utils.JsonUtils;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Repository
public class RedisServiceSimple {
    @Resource(name = "redisTemplate")
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private ValueOperations<String,String> operations;

    public <T> T get(String prefix, String key, Class<T> clazz) {
        String val = operations.get(prefix+key);
        return JsonUtils.parseJsonString(val,clazz);
    }

    public <T> void set(String prefix, String key, T value) {
        operations.set(prefix+key,JsonUtils.toJsonString(value));
    }

    public <T> List<T> multiGet(Set<String> keysWithPrefix, Class<T> clazz) {
        List<String> temp = operations.multiGet(keysWithPrefix);
        List<T> res = new ArrayList<T>();
        if (temp==null) {
            return res;
        }
        for (String s : temp) {
            res.add(JsonUtils.parseJsonString(s, clazz));
        }
        return res;
    }
}
