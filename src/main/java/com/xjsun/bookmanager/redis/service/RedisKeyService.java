package com.xjsun.bookmanager.redis.service;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;

@Repository
public class RedisKeyService {

    @Resource(name = "redisTemplate")
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private RedisTemplate<String, Object> redisTemplate;

    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * pattern: Books, suffix: *
     * -> keys Books*
     */
    public Set<String> keys(String pattern, String suffix) {
        return redisTemplate.keys(pattern+suffix);
    }
    /**
     * prefix: [, pattern: Books, suffix: ]
     * -> keys [Books]
     */
    public Set<String> keys(String prefix, String pattern, String suffix) {
        return redisTemplate.keys(prefix+pattern+suffix);
    }
}
