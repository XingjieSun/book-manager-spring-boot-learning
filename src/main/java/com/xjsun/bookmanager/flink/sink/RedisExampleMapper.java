package com.xjsun.bookmanager.flink.sink;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommand;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisCommandDescription;
import org.apache.flink.streaming.connectors.redis.common.mapper.RedisMapper;

public class RedisExampleMapper implements RedisMapper<Tuple2<String, String>> {
    @Override
    public RedisCommandDescription getCommandDescription() {
        return new RedisCommandDescription(RedisCommand.HSET, "HASH_REDIS_KEY");
    }

    @Override
    public String getKeyFromData(Tuple2<String, String> data) {
        return data.f0;
    }

    @Override
    public String getValueFromData(Tuple2<String, String> data) {
        return data.f1;
    }
}
/**
 * 官网的例子，相当于在redis创建一个key为  HASH_REDIS_KEY的Hash结构
 * 然后告诉Flink如何把Stream里面的数据提取出key和value
 * 最后自动存入这个hash
 * 注意，这个是Mapper  不是Sink本身
 *
 * FlinkJedisPoolConfig conf = new FlinkJedisPoolConfig.Builder().setHost("127.0.0.1").build();
 *
 * DataStream<String> stream = ...;
 * stream.addSink(new RedisSink<Tuple2<String, String>>(conf, new RedisExampleMapper());
 */
