package com.xjsun.bookmanager.flink;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.HttpAsyncClient;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public class RichAsyncFuncDemo extends RichAsyncFunction<String, String> {
    private static CloseableHttpAsyncClient client;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(1000)
                .setConnectTimeout(1000).build();
        client = HttpAsyncClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setMaxConnTotal(20)
                .disableCookieManagement().build();
        client.start();
    }

    @Override
    public void close() throws Exception {
        super.close();
        client.close();
    }

    @Override
    public void asyncInvoke(String input, ResultFuture<String> resultFuture) throws Exception {
        HttpGet httpGet = new HttpGet("http://localhost:8080/test/asyncGetTest?key="+input);
        Future<HttpResponse> responseFuture = client.execute(httpGet, null);
        CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                String res = null;
                try {
                    HttpResponse response = responseFuture.get();
                    if (response.getStatusLine().getStatusCode() == 200) {
                        res = response.getEntity().toString();
                    }
                    return res;
                } catch (Exception e) {
                    return null;
                }
            }
        }).thenAccept(res -> resultFuture.complete(Collections.singleton(res + "modified by accept")));
    }

}
