package com.xjsun.bookmanager.flink;

public class FlinkConstConfigs {
    public static final String CHECK_POINT_DIR_LOCAL = "file:///Users/sunxingjie/projects/book-manager-spring-boot-learning/checkpoint";

    public static final String LOCAL_HOST = "localhost";
    public static final int SOCKET_STREAM_PORT = 8888;


}
