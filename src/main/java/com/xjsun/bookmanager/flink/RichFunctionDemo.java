package com.xjsun.bookmanager.flink;

import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.utils.SpringUtils;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

public class RichFunctionDemo extends RichMapFunction<String, Book> {
    /**
     * Flink收到信息格式为  timestamp, bookId, authorId
     * 需要根据authorId去redis查出作者信息，设置为author对象
     */

    private RedisTemplate redisTemplate;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        redisTemplate= SpringUtils.getBean("redisTemplate",RedisTemplate.class);

    }

    @Override
    public void close() throws Exception {
        super.close();
        redisTemplate=null;
    }

    @Override
    public Book map(String value) throws Exception {
        String[] temp = value.split(",");
        Author a = null;
        if (temp.length==3) {
            a = (Author) redisTemplate.opsForValue().get(temp[2]);
        }
        if (a==null) {
            a = new Author(-1,"unknown");
        }
        return new Book(Integer.parseInt(temp[1]),"",a,null,null,null);
    }
}
