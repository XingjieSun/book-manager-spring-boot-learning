package com.xjsun.bookmanager.flink;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.windowing.RichWindowFunction;
import org.apache.flink.util.Collector;

/**
 * 关于keyedState的使用，官网给了countwindowAvg的例子，这边写一个max来复习
 * 值得一提的是这个countWindow不是api
 * 而是FlatMap实现一个多输入单输出的结果
 * 在flatMap的成员变量里面保存stream的数据，达到一定数量后出一个结果
 * ds.flatMap(new CountWindowMax)
 *
 * 如果要在真正的window后面使用，需要继承Aggregator
 * */
public class CountWindowMax extends RichFlatMapFunction<Tuple2<String, Integer>,Integer> {
    private transient ValueState<Tuple2<Integer, Integer>> curMaxCurCount;
    private static final int Window_Size=5;

    @Override
    public void open(Configuration parameters) throws Exception {
        ValueStateDescriptor<Tuple2<Integer, Integer>> descriptor = new
                ValueStateDescriptor<Tuple2<Integer, Integer>>("current max", Types.TUPLE(Types.INT,Types.INT));
        curMaxCurCount = getRuntimeContext().getState(descriptor);
    }


    @Override
    public void flatMap(Tuple2<String, Integer> value, Collector<Integer> out) throws Exception {
        Integer temp = value.f1;
        Tuple2<Integer, Integer> state = curMaxCurCount.value();
        if (state==null) {
            state = Tuple2.of(temp,0);
        }
        state.f1+=1;
        if (state.f0 < temp) state.f0 = temp;
        if (state.f1 == Window_Size) {
            out.collect(state.f0); // 记录当前最大值
            curMaxCurCount.clear(); // 本次window结束 清空
        } else {
            curMaxCurCount.update(state);
        }
    }
}
