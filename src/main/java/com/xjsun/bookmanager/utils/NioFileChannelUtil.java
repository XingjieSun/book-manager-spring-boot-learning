package com.xjsun.bookmanager.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

public class NioFileChannelUtil {
    public static String readTxt(Path txtPath) {
        String res = null;
        try {
            // 缓冲区大小 字节数
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            FileChannel inFc = new FileInputStream(txtPath.toString()).getChannel();
            buffer.clear();
            int length = inFc.read(buffer);
            res = new String(buffer.array(), 0, length, StandardCharsets.UTF_8);
            inFc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean writeTxt(Path txtPath, String content, boolean append) {
        try {
            FileChannel outFc = new FileOutputStream(txtPath.toString(), append).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.wrap(content.getBytes(StandardCharsets.UTF_8));
            outFc.write(byteBuffer);
            outFc.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
