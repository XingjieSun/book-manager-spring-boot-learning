package com.xjsun.bookmanager.utils;

import com.alibaba.fastjson.JSON;

public class JsonUtils {
    public static String toJsonString(Object obj){ return JSON.toJSONString(obj);}

    public static <T> T parseJsonString(String jsonString, Class<T> clazz){
        if (jsonString == null || jsonString.length() <= 0 || clazz == null) {
            return null;
        } else if (clazz == String.class) {
            return (T) jsonString;
        } else {
            return JSON.parseObject(jsonString, clazz);
        }
    }
}
