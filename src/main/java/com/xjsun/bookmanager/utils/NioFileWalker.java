package com.xjsun.bookmanager.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.xjsun.bookmanager.utils.NioFileChannelUtil.readTxt;

public class NioFileWalker {
    public static void testWalker() {
        try (Stream<Path> paths = Files.walk(Paths.get("E:\\杂乱存储\\testdir1"))) {
            paths.map(Path::getFileName).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return key: NewDisk目录的字符串， value: DiskName
     */
    public static Map<String, String> getPossibleDrives() {
        List<String> drives = Arrays.asList("C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "U");
        Map<String, String> map = new HashMap<>(2);
        for (String x : drives) {
            x += ":\\NewDisk整理结果";
            Path p = Paths.get(x);
            if (Files.exists(p) && Files.exists(p.resolve("DiskName.txt"))) {
                String s = readTxt(p.resolve("DiskName.txt"));
                if (s != null) {
                    map.put(p.toString(), s);
                }
            }
        }
        return map;
    }


}
