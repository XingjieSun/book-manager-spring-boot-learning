package com.xjsun.bookmanager.utils;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;


/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/12/28 20:48
 */
public class FlinkKafkaUtils {
    private static StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    public static StreamExecutionEnvironment getEnv() {
        return env;
    }

    public static <T> DataStream<T> getKafkaDataStream(ParameterTool params, Class<? extends DeserializationSchema<T>> clazz) throws IllegalAccessException, InstantiationException {
        KafkaSource<T> source = KafkaSource.<T>builder()
                .setBootstrapServers(params.getRequired("bootstrapServers"))
                .setTopics(Arrays.asList(params.getRequired("topics").split(",")))
                .setGroupId(params.getRequired("groupId"))
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(clazz.newInstance())
                .build();
        return env.fromSource(source, WatermarkStrategy.noWatermarks(), params.getRequired("sourceName"));
    }
}
