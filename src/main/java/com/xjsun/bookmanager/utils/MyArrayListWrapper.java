package com.xjsun.bookmanager.utils;

import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/16 13:36
 */
@NoArgsConstructor
public class MyArrayListWrapper<T> {
    public List<T> list;
    public int pages = 1;
    public int pageNum = 1;

    public MyArrayListWrapper(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int[] getNavigatepageNums() {
        return new int[]{1};
    }
}
