package com.xjsun.bookmanager.utils;

import java.io.Serializable;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/4/11 21:15
 */
public class Result implements Serializable {
    private String code;
    private String msg;
    private Object data;

    public static Result suc(Object data) {
        return new Result("0", "success", data);
    }

    public static Result fail(Object data) {
        return new Result("-1", "fail", data);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Result() {
    }

    public Result(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
