package com.xjsun.bookmanager.utils;

import com.xjsun.bookmanager.aspectlogger.MyAspectLogger;

public class TestAopOnStaticFunc {
    @MyAspectLogger
    public static String testAopOnFunc() {
        return "AOP On Static Func: 没有效果，因为方法不是bean";
    }
}
