package com.xjsun.bookmanager.utils;

import com.xjsun.bookmanager.aspectlogger.MyAspectLogger;
import com.xjsun.bookmanager.aspectlogger.ToRedisCache;
import com.xjsun.bookmanager.pojo.User;
import org.springframework.stereotype.Component;

@Component
public class TestAopOnComponent {
    @MyAspectLogger
    public static String testAopOnStaticFuncInComponent() {
        return "testAopOnStaticFuncInComponent";
    }

    @MyAspectLogger
    public String testAopOnFuncInComponent() {
        return "testAopOnNotStaticFuncInComponent";
    }

    @ToRedisCache
    public User testAopToRedisCache(int id) {
        return new User(id, "u"+id, "pwdddd");
    }

    @ToRedisCache
    public User testAopToRedisCachePoint2(int id) {
        return new User(id, "2u"+id, "2pwdddd");
    }
    // Around>被切方法>AfterRunning>Around

    @MyAspectLogger
    @ToRedisCache
    public User testAopToRedisCachePoint3(int id) {
        return new User(id, "3u"+id, "3pwdddd");
    }
    // 输出结果如下
    //Enter into method: testAopToRedisCachePoint3
    //Method_testAopToRedisCachePoint3
    //testAopToRedisCachePoint3 returns: User(id=333, name=3u333, pwd=3pwdddd)
    //After method: testAopToRedisCachePoint3
}
