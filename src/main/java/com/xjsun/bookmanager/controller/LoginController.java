package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.config.ProjectInfo;
import com.xjsun.bookmanager.mapper.UserMapper;
import com.xjsun.bookmanager.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/14 20:29
 */

@Controller
public class LoginController {
    @Autowired private UserMapper um;
    @Autowired private ProjectInfo pi;

    private int checkUser(String uName,String uPw){
        User temp = um.getUserByName(uName);
        if (temp==null){
            System.out.println("用户不存在 -1");
            return -1;
        }
        if (uPw.equals(temp.getPwd())){
            System.out.println("登录成功 0");
            return 0;
        }
        else {
            System.out.println("密码错误 1");
            return 1;
        }
    }
    private void loadInfo(Model m){
        m.addAttribute("pjName", pi.getProjectName());
        m.addAttribute("pjDsc",pi.getLoginDescription());
    }

    @GetMapping("/login")
    public String loginPage(Model m){
        loadInfo(m);
        return "login";
    }

//    @RequestMapping("/doLogin")
    @PostMapping("/doLogin")
    public String login(Model m, HttpSession session,
                        @RequestParam("loginUsername") String uName,
                        @RequestParam("loginPassword") String uPw){
        // 登录过程
        //判断用户名和密码的合法性
        int loginCode = checkUser(uName,uPw);
        if (loginCode==0){
            session.setAttribute("loginUser",uName);
            return "redirect:main.html";
        }
        else if (loginCode==1) {
            m.addAttribute("msg","密码错误");
            loadInfo(m);
            return "login";
        }
        else if (loginCode==-1) {
            m.addAttribute("msg","用户名不存在");
            loadInfo(m);
            return "login";
        }
        else {
            m.addAttribute("msg","未知错误");
            loadInfo(m);
            return "login";
        }
    }

    @RequestMapping("/doLogout")
    public String logout(HttpSession session){
        System.out.println("Logout");
        session.invalidate();
        return "redirect:login";
    }
}
