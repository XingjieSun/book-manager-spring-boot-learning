package com.xjsun.bookmanager.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xjsun.bookmanager.mapper.AuthorMapper;
import com.xjsun.bookmanager.mapper.BookCategoryMapper;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.mapper.CategoryMapper;
import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/21 20:29
 */
@Controller
public class CategoriesController {
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    BookCategoryMapper bookCategoryMapper;
    @Autowired
    BookMapper bookMapper;

    @GetMapping("/categories.html")
    public String categoriesPage(Model m, @RequestParam(value = "id", defaultValue = "0") int id,
                                 @RequestParam(value = "pn",defaultValue = "1") int pn){
        List<Category> categories = categoryMapper.findAll();
        List<Book> books = null;
        PageInfo<Book> pageInfo = null;
        Category currentCate = null;
        String currentTitle="All";
        m.addAttribute("categoryList",categories);
        if (id>0){
            currentCate = categoryMapper.getCategoryById(id);

            if(currentCate!=null){
                currentTitle=currentCate.getName();
                List<Integer> bookIdList=bookCategoryMapper.getBookIdListByCategory(currentCate);
                if (bookIdList!=null){
                    PageHelper.startPage(pn,10);
                    books=bookMapper.getBookListByIdSet(bookIdList);
                    pageInfo = new PageInfo<>(books,5);
                    System.out.println("查询到相关书目："+books.size());

                }
            }
        }
        // 仅仅在categories模板里用到 ，  为了在一堆标签里面显示当前选中的标签
        m.addAttribute("title",currentTitle);
        m.addAttribute("currentId",id);
        // 展示book
        m.addAttribute("bookListPage",pageInfo);
        m.addAttribute("urlName","categories.html?id="+id+"&");
        // 下面三个是弹出表单
        m.addAttribute("myObj",currentCate);
        m.addAttribute("myObjType","Category");
        m.addAttribute("theAction","/updateCategory");
        return "myTemps/categories";
    }


}
