package com.xjsun.bookmanager.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.pojo.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/21 18:46
 */
@Controller
public class LibraryController {
    @Autowired
    BookMapper bookMapper;
    private static Logger logger = LogManager.getLogger(LibraryController.class.getName());
    @GetMapping("/library")
    public String libraryPage(Model m, @RequestParam(value = "pn", defaultValue = "1") int pn) {
        logger.warn("pn:" + pn);
        List<Book> bookList = null;

        PageHelper.startPage(pn, 15);
        bookList = bookMapper.findAll();
        PageInfo<Book> page = new PageInfo<>(bookList, 5);
        m.addAttribute("bookListPage", page);
        m.addAttribute("urlName", "library?");
        return "myTemps/library";

    }
}
