package com.xjsun.bookmanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/5/22 18:47
 */
@Controller
public class AjaxLearningController {
    @GetMapping("/AjaxTest")
    public String ajaxTest() {
        return "page4test/ajaxTest1";
    }

    @GetMapping("/AjaxTest2")
    public String ajaxTest2() {
        return "page4test/ajaxTest2";
    }

    @RequestMapping("/AjaxLearning")
    public void ajaxLearning1(HttpServletResponse response, String username, String password) throws IOException {
        System.out.println("ajaxLearning");
        // service
        Map<String, String> map = myService(username, password);
        // to json
        String jsonObj = new ObjectMapper().writeValueAsString(map);
        // return results to HttpResponse
        response.setContentType("application/json;charset=utf-8");
        PrintWriter pw = response.getWriter();
        pw.println(jsonObj);
        pw.flush();
        pw.close();
    }

    private Map<String, String> myService(String username, String password) {
        Map<String, String> map = null;
        if (password != null) {
            map = new HashMap<>();
            map.put("uName", username);
            map.put("pwd", "XXXXXX");
        }
        return map;
    }
}
