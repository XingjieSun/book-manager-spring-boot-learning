package com.xjsun.bookmanager.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xjsun.bookmanager.mapper.AuthorMapper;
import com.xjsun.bookmanager.mapper.BookMapper;
import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.utils.MyArrayListWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/21 20:29
 */
@Controller
public class AuthorsController {
    @Autowired
    AuthorMapper authorMapper;
    @Autowired
    BookMapper bookMapper;
    private static Logger logger = LogManager.getLogger(AuthorsController.class.getName());

    @GetMapping("/listAuthorsJson")
    @ResponseBody
    public List<Author> listAuthorsJson() {
        logger.info("listAuthorsJson");
        List<Author> authors = authorMapper.findAll();
        logger.info("authors num: " + authors.size());
        return authors;
    }

    @GetMapping("/authors.html")
    public String authorsPage(Model m, @RequestParam(value = "pn", defaultValue = "1") int pn) {
        PageHelper.startPage(pn, 9);
        logger.info("pn=" + pn);
        List<Author> authors = authorMapper.findAll();
        PageInfo<Author> pageInfo = new PageInfo<>(authors, 5);
        m.addAttribute("authorListPage", pageInfo);
        m.addAttribute("urlName", "authors.html?");
        m.addAttribute("defaultFace", "img/face_girl.png");
        return "myTemps/authors";
    }

    @GetMapping("/author/{id}")
    public String author(Model m, @PathVariable(value = "id") int id) {
        // 这里不需要pageHelper 但依然使用library里面的展示book的模板
        Author a = authorMapper.getAuthorById(id);
        m.addAttribute("curAuthor", a);
        logger.info("id=" + id);
        List<Book> books = bookMapper.getBookListByAuthor(a);
        // 展示book 使用了模板 <div class="table-responsive" th:fragment="libraryBookList">
        MyArrayListWrapper<Book> wrappedBooks = new MyArrayListWrapper<Book>(books);
        m.addAttribute("bookListPage", wrappedBooks);
        m.addAttribute("urlName", "author/" + id + "/?");
        // <div th:replace="~{commons/myForms::modal_form_edit_CategoryOrAuthor}"></div>
        m.addAttribute("myObj", a);
        m.addAttribute("myObjType", "Author");
        m.addAttribute("theAction", "/updateAuthor");
        return "/myTemps/authorDetails";
    }

}
