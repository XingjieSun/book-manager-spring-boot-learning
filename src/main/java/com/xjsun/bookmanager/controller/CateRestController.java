package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.mapper.CategoryMapper;
import com.xjsun.bookmanager.mapper.impl.CategoryMapperImpl;
import com.xjsun.bookmanager.pojo.Category;
import com.xjsun.bookmanager.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/4/11 19:17
 */

@RestController
@RequestMapping("/test")
public class CateRestController {
    @Autowired
    CategoryMapperImpl categoryMapperWithCache;
    @Autowired
    CategoryMapper categoryMapper;

    /**
     * 有Cache的版本反而更慢    不知道是不是win的原因
     */
    @GetMapping("/Cache1/{id}")
    public Result testCache1(@PathVariable(name = "id") int id) {
        long startTime = System.currentTimeMillis();
        Category c = categoryMapperWithCache.getCategoryById(id);
        for (int i = 0; i < 10; i++) {
            categoryMapper.getCategoryById(id + i);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(c.getName());
        String msg = (endTime - startTime) + "ms";
        return Result.suc(msg);
    }

    @GetMapping("/Cache2/{id}")
    public Result testCache2(@PathVariable(name = "id") int id) {
        long startTime = System.currentTimeMillis();
        Category c = categoryMapper.getCategoryById(id);
        for (int i = 0; i < 10; i++) {
            categoryMapper.getCategoryById(id + i);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(c.getName());
        String msg = (endTime - startTime) + "ms";
        return Result.suc(msg);
    }
}
