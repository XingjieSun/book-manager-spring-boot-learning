package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.service.MyMails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/3/7 19:48
 */
@RestController
public class AsynchronousTestController {
    @Autowired
    MyMails myMails;

    @GetMapping("/asyncTest")
    public String asyncTest() {
        myMails.testAsync();
        try {
            myMails.sendMail("试试看发邮件", "springboot mail", true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ok";
    }

    /**
     * 只是给Flink异步处理HttpAsyncClient的get请求提供一个网址来访问
     */
    // HttpGet httpGet = new HttpGet("localhost:8080/test/asyncGetTest?key="+input);
    @GetMapping("/test/asyncGetTest")
    public String asyncTestForFlink(@RequestParam(value = "key", defaultValue = "default") String key) throws InterruptedException {
        String res = "Async Test:"+ key + " Wait:";
        int randWaitMs = new Random(0).nextInt(1000);
        Thread.sleep(randWaitMs);
        return res+randWaitMs;
    }
}
