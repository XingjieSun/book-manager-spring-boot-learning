package com.xjsun.bookmanager.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/7/26 18:02
 */
@RestController
public class HelloController {
    @PostMapping("/test/hello")
    public Map<String, String> hello(@RequestBody Map<String, String> map) {
        map.put("after hello", new Date().toString());
        return map;
    }
}
