package com.xjsun.bookmanager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/14 21:31
 */
@Controller
public class IndexController {
    @RequestMapping("/userIndex")
    public String index(Model m){
        return "index";
    }
}
