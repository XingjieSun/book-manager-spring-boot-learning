package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Book;
import com.xjsun.bookmanager.redis.keyPrefix.BookPrefix;
import com.xjsun.bookmanager.redis.service.RedisKeyService;
import com.xjsun.bookmanager.redis.service.RedisServiceSimple;
import com.xjsun.bookmanager.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
public class HotBooksController {
    private static String hotBookPrefix;
    @Autowired
    RedisServiceSimple redisServiceSimple;
    @Autowired
    RedisKeyService redisKeyService;

    @GetMapping("/hotBooks")
    public List<Book> hotBooks() {
        hotBookPrefix = BookPrefix.getBooksDetailPrefix().getPrefix();
//        ArrayList<Book> bookArrayList = new ArrayList<>();
        Set<String> keySet = redisKeyService.keys(hotBookPrefix, "*");
        List<Book> bookArrayList = redisServiceSimple.multiGet(keySet, Book.class);

        return bookArrayList;
//        return new Book();
    }

    @GetMapping("/hotBooks1")
    public String hotBooks1() {
        hotBookPrefix = BookPrefix.getBooksDetailPrefix().getPrefix();
        ArrayList<Book> bookArrayList = new ArrayList<>();
        bookArrayList.add(new Book());
        bookArrayList.add(new Book());
        Book b = new Book();
        b.setDate("12345");
        b.setAuthor(new Author());
        b.setTitle("Title B");
        bookArrayList.add(b);
        return JsonUtils.toJsonString(bookArrayList);
//        return "123";

    }

    @GetMapping("/hotBooks2")
    public List<Book> hotBooks2() {
        hotBookPrefix = BookPrefix.getBooksDetailPrefix().getPrefix();
        ArrayList<Book> bookArrayList = new ArrayList<>();
        bookArrayList.add(new Book());
        bookArrayList.add(new Book());
        return bookArrayList;

    }
}
