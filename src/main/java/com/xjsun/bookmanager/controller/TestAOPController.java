package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.aspectlogger.MyAspectLogger;
import com.xjsun.bookmanager.utils.TestAopOnComponent;
import com.xjsun.bookmanager.utils.TestAopOnStaticFunc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class TestAOPController {
    @Autowired
    TestAopOnComponent aopOnComponent;


    @RequestMapping("/test/aop1")
    @MyAspectLogger
    public String testAop1() {
        return "aop1";
    }

    @RequestMapping("/test/aop2")
    public Map<String,String> testAop2() {
        // 虽然是controller里面的方法，但是因为没有bean，所以无效
        return testAopOnMethod();
    }
    @MyAspectLogger
    public Map<String,String> testAopOnMethod() {
        Map<String, String> map = new HashMap<>();
        map.put("aop","test2");
        return map;
    }
    @RequestMapping("/test/aop3")
    public String testAop3() {
        // 静态方法没有bean  无效
        return TestAopOnStaticFunc.testAopOnFunc();
    }
    @RequestMapping("/test/aop4")
    public Map<String,String> testAop4() {
        // 即使类上有Component，静态方法调用时也不会用到bean， 不能被增强
        Map<String, String> map = new HashMap<>();
        map.put("aop",TestAopOnComponent.testAopOnStaticFuncInComponent());
        return map;
    }
    @RequestMapping("/test/aop5")
    public Map<String,String> testAop5() {
        //
        Map<String, String> map = new HashMap<>();
        map.put("aop",aopOnComponent.testAopOnFuncInComponent());
        return map;
    }



}
