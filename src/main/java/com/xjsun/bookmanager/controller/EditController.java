package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.mapper.AuthorMapper;
import com.xjsun.bookmanager.mapper.CategoryMapper;
import com.xjsun.bookmanager.pojo.Author;
import com.xjsun.bookmanager.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/21 21:57
 */
@Controller
public class EditController {
    @Autowired
    AuthorMapper authorMapper;
    @Autowired
    CategoryMapper categoryMapper;
    // http://localhost:8080/editAuthor/1
    @RequestMapping("/editAuthor/{id}")
    public String authorEditor(Model m, @PathVariable(value = "id") int id){
        Author x = authorMapper.getAuthorById(id);
//        List<String> titles = authorMapper.getBookTitlesByAuthorId(id);

        m.addAttribute("title","Author");
        m.addAttribute("formName","authorForm");
        m.addAttribute("author",x);
//        m.addAttribute("bookTitles",titles);
        return "myTemps/editPage";
    }
    @PostMapping("/updateAuthor")
    public String updateAuthor(Author a){
        System.out.println("Update Author");
        System.out.println(a);
        authorMapper.updateAuthor(a);
        return "redirect:/authors.html";
    }

    @GetMapping("/editCategory/{id}")
    public String categoryEditor(Model m, @PathVariable(value = "id") int id){
        Category x = categoryMapper.getCategoryById(id);
        m.addAttribute("title","Category");
        m.addAttribute("formName","categoryForm");
        m.addAttribute("category",x);
        return "myTemps/editPage";
    }
    @PostMapping("/updateCategory")
    public String updateCategory(Category c){
        System.out.println("Update Cate");
        System.out.println(c);
        categoryMapper.updateCategory(c);
        return "redirect:/categories.html";
    }
}
