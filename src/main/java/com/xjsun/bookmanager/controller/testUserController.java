package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.mapper.UserMapper;
import com.xjsun.bookmanager.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/18 23:04
 */
@RestController
public class testUserController {
    @Autowired
    UserMapper userMapper;

    @GetMapping("/testFindAllUsers")
    public List<User> findAllUsers(){
        List<User> userList = userMapper.findAll();
        System.out.println(userList.size());
        return userList;
    }

}
