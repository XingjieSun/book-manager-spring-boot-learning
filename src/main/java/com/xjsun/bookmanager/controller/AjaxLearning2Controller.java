package com.xjsun.bookmanager.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/5/27 15:50
 */
@RestController
public class AjaxLearning2Controller {
    @PostMapping("/AjaxLearning2")
    public Object ajaxLearning2(String username, String password) throws JsonProcessingException {
        System.out.println("ajaxLearning 2");
        Map<String, String> res = myService(username, password);
        return res;
    }

    private Map<String, String> myService(String username, String password) {
        Map<String, String> map = null;
        if (password != null) {
            map = new HashMap<>(2);
            map.put("uName", username);
            map.put("pwd", "XXXXXX");
        }
        return map;
    }
}
