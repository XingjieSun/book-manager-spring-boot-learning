package com.xjsun.bookmanager.controller;

import com.xjsun.bookmanager.config.ProjectInfo;
import com.xjsun.bookmanager.mapper.UserMapper;
import com.xjsun.bookmanager.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * @author Xingjie Sun
 * @version 1.0
 * @date 2021/2/19 14:00
 */
@Controller
public class RegisterController {
    @Autowired
    private ProjectInfo pi;

    @Autowired private UserMapper um;

    private void loadInfo(Model m){
        m.addAttribute("pjName", pi.getProjectName());
        m.addAttribute("pjDsc",pi.getRegisterDescription());
    }

    @GetMapping("/register")
    public String registerPage(Model m){
        loadInfo(m);
        return "register";
    }

    @PostMapping("/register")
    public String doRegister(@RequestParam("registerUsername") String uName,
                             @RequestParam("registerEmail") String uEmail,
                             @RequestParam("registerPassword") String uPwd,
                             Model m){
        if(checkUserName(uName)){
            System.out.println("用户名可以使用");
            um.addUser(new User(1, uName, uPwd));
            return "redirect:/login";
        }
        else{
            System.out.println("用户名已被注册");
            m.addAttribute("msg","用户名已被注册");
            loadInfo(m);
            return "register";
        }
    }

    private boolean checkUserName(String uName){
        User temp = um.getUserByName(uName);
        return temp==null;
    }
}
