
sites = {
    "3dm_hanhua" :"https://www.3dmgame.com/hanhua/",
    "3dm_zq" :"https://www.3dmgame.com/games/zq",
}

xpaths_dict = {
    "3dm_hanhua" :{
    'items' :'//div[@class="cent"]/a',
    'item2name' :'./span/text()',
    'item2cate' :'./i/text()',
    'item2href' :'./@href'
    },
    "3dm_zq" :{
        'items' :'//div/div[@class="lis"]',
        'item2name' :'./a[2]/text()',
        # 'item2cate':'./i/text()',
        'item2href' :'./a[2]/@href',
        'item2info' :'./ul/li/text()',
        'item2score' :'./div[@class="pfbox"]/div/div/font/text()',
        'nextPage':'//div[@class="pagewrap"]/ul/li[@class="next"]/a/@href'
    }
}
headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.74'
}

dbConfigs={
        'host': '127.0.0.1',
        'user': 'root',
        'passwd': 'admin',
        'database': 'bookmanager',
        'auth_plugin': 'mysql_native_password'
}