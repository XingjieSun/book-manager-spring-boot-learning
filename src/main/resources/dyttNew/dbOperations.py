import mysql.connector
from sDmConfig import *
from utils import *
from runSDMspider import *

class UseDataBase:
    # with 不自动commit  为了一次连接做多个sql
    def __init__(self, config: dict):
        self.configuration = config

    def __enter__(self):
        self.conn = mysql.connector.connect(**self.configuration)
        self.cursor = self.conn.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


def insertAuthors(authors: set):
    sql = """ insert ignore into  author (name) values (\"{}\"); """
    with UseDataBase(config=dbConfigs) as cursor:
        for aName in authors:
            cursor.execute(sql.format(aName))


def insertCategories(categories: set):
    sql = """ insert ignore into  category (name) values (\"{}\"); """
    with UseDataBase(config=dbConfigs) as cursor:
        for cName in categories:
            cursor.execute(sql.format(cName))


def insertItems(items: list):
    # title, date, authorName
    sql_book = """ insert ignore into book (title, date, url, author_id) values
                    (\"{}\", \"{}\" ,  \"{}\" ,  
                        (select a.id from author a where a.`name`= \"{}\" limit 1)
                    );"""
    # title, cateName
    sql_bc = """ insert into book_category (book_id,category_id) values
                (
                    (SELECT b.id from book b where b.`title`= \"{}\" limit 1),
                    (select c.id from category c where c.`name`= \"{}\" limit 1)
                );"""
    #  temp = [name, href, date, author, cate, score]
    for item in items:
        with UseDataBase(config=dbConfigs) as cursor:
            cursor.execute(sql_book.format(item[0], item[2],item[1],item[3]))
        with UseDataBase(config=dbConfigs)as cursor:
            cursor.execute(sql_bc.format(item[0], item[4]))

def startInsert(fPath,startId=0, offset=-1):
    items = load(fPath)
    items, authors, categories = processDataBeforeInsert(items,startId,offset)
    print("准备插入记录数量："+str(len(items)))
    insertAuthors(authors)
    insertCategories(categories)
    insertItems(items)

if __name__ == '__main__':
    fPath = "data/{}-pn{}_{}.json".format(key, startPn, endPn)
    startInsert(fPath,0)

