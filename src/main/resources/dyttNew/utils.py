import json


def save(data, fPath):
    with open(fPath, 'w', encoding='utf-8')as f:
        json.dump(data, f)


def load(fPath):
    with open(fPath, 'r', encoding='utf-8')as f:
        return json.load(f)


def processDataBeforeInsert(items: list,startId=0, offset=-1):
    #  temp = [name, href, date, author, cate, score]
    authors = set()
    categories = set()
    for item in items[startId:offset:]:
        authors.add(item[3])
        categories.add(item[4])
    return items[startId:offset:], authors, categories

def showList():
    items = load("data/3dm_zq-pn1_10.json")
    for x in items:
        print(x)
if __name__ == '__main__':
    showList()


