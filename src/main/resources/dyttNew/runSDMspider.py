from sDmSpider import *
from utils import save

startPn = 1
endPn = 20
key = '3dm_zq'
printProcess = False
minScore=4.0

if __name__ == '__main__':


    res_dict = {}
    for pn in range(startPn, endPn):
        res_dict[pn] = parsePage(key, pn=pn, printProcess=printProcess)
    finalRes=[]
    for pn in sorted(res_dict):
        for item in res_dict.get(pn):
            if float(item[-1] )> minScore:
                finalRes.append(item)
    save(finalRes, "data/{}-pn{}_{}.json".format(key,startPn,endPn))