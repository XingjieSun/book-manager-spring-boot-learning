testSites=["https://www.80s.tw/","https://www.dytt.com/","https://www.ygdy8.net/","https://www.3dmgame.com/hanhua/"]


sites = {
    # 7具体是什么不确定  看起来是分页查询  .format(pageN) 即可
    "oumei":"https://www.ygdy8.net/html/gndy/oumei/list_7_{}.html",
    "usa":"https://www.dytt.com/fenlei/1.html",
    "3dm_hanhua":"https://www.3dmgame.com/hanhua/"
}

xpaths = {
    "movie_list":'//*[@id="header"]/div/div[3]/div[3]/div[2]/div[2]/div[2]/ul',
    # /html/body/div/div/div[3]/div[3]/div[2]/div[2]/div[2]/ul/table[1]
    # /html/body/div/div/div[3]/div[3]/div[2]/div[2]/div[2]/ul/table[1]
    "movie_list_items":'/html/body/div/div/div[3]/div[3]/div[2]/div[2]/div[2]/ul/table',
    # /html/body/div/div/div[3]/div[3]/div[2]/div[2]/div[2]/ul/table[1]/tbody/tr[2]/td[2]/b/a[2]
    "item_to_href":'./tbody/tr[2]/td[2]/b/a[2]/@href',
    "item_to_title":'./tbody/tr[2]/td[2]/b/a[2]/text()'
}
headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.74'
}