import requests
from lxml.html import etree
from sDmConfig import *
from saveHtml import saveHtml




def parsePage(key,pn=1,printProcess=False):
    html = getPage(sites.get(key)+"_{}".format(pn))
    xpaths = xpaths_dict.get(key)
    ls = html.xpath(xpaths.get('items'))
    print('正在处理： pn={}，item={}'.format(pn,len(ls)))
    res=[]
    for item in ls:
        href = item.xpath(xpaths.get('item2href'))[0]
        name = item.xpath(xpaths.get('item2name'))[0]
        score = item.xpath(xpaths.get('item2score'))[0]
        info_ls = item.xpath(xpaths.get('item2info'))
        date, author, cate = parseInfo_zq(info_ls)
        temp = [name, href, date, author, cate, score]
        temp = list(map(lambda x: x.strip(), temp))
        res.append(temp)
        if printProcess: print(temp)
    return res


def getPage(url):
    resp = requests.get(url, headers=headers)
    saveHtml(resp, 'url')
    html = etree.HTML(resp.text)
    return html


def testStep(key):
    html = getPage(sites.get(key))
    xpaths = xpaths_dict.get(key)
    ls = html.xpath(xpaths.get('items'))
    # ls = html.xpath('//div/div/a')
    assert len(ls) > 0
    print(len(ls))
    for item in ls:
        href = item.xpath(xpaths.get('item2href'))[0]
        name = item.xpath(xpaths.get('item2name'))[0]
        score = item.xpath(xpaths.get('item2score'))[0]
        info_ls = item.xpath(xpaths.get('item2info'))
        date, author, cate = parseInfo_zq(info_ls)
        temp = [name, href, date, author, cate, score]
        temp = list(map(lambda x:x.strip(),temp))
        print(temp)

def getNextPage(html,key):
    xpaths = xpaths_dict.get(key)
    url = html.xpath(xpaths.get('nextPage'))
    return getPage(url)


def parseInfo_zq(ls: list):
    # 一些 li 标签的字符串
    try:
        date = ls[0].split("发售：")[1].split(' ')[0]
        author = ls[2].split("开发：")[1]
        cate = ls[4].split("类型：")[1]

    except Exception as e:
        print("parseInfo_zq 出现异常")
        print(e)
        return ['', '', '']
    return [date, author, cate]


if __name__ == '__main__':
    # testStep('3dm_hanhua')
    testStep('3dm_zq')
